#!/bin/bash

# get dependancies 
yum -y install epel-release
yum -y install \
   docker docker-compose git

# ensure docker is running
systemctl start docker
systemctl enable docker

# make directories (if they do not already exist) to store database and other rocket.chat items 
mkdir -p data/ db/ rocketchat/ uploads/ 

# ensure proper permissions on the folders
chcon  -R -t svirt_sandbox_file_t *
